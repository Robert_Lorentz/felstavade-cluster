import sys
import re
import os
from Levenshtein import distance
from subprocess import call

autoList = ["utbildning","frågor","kunskap","kunskaper","examen","erfaren","erfarenhet","erfarenheter"
            ,"vana","arbete","arbeten","körning","körningar","intresse","teknik","tekniker","produktion","produktioner"
            ,"tjänst","tjänster","behandling","behandlingar","anläggning","anläggningar","utrustning","montering","metod"
            ,"metoder","logi","installation","installationer","hantering","analys","analyser","utveckling"
            ,"system","verktyg","kommunikation","certifikat","industri","industrier","makeri","makerier","smide"
            ,"planering","tillverkning","tillverkningar","ekonomi","rätt","sjukvård","system","kemi","fysik","juridik"
            ,"konstruktion","konstruktioner","merit","meriter","vetenskap","vetenskaper","program","programmering","teori","teorier"
            ,"service","forskning","undervisning","legitimation","kompetens","ansvar","handledning","uppgifter","verkstad"
            ,"maskin","maskiner","utredning","utredningar","redovisning","redovisningar","statistik","rapportering"
            ,"rapporteringar","verksamhet","förmåga","förmågor","mätning","mätningar","apotek","vård","medicin","mediciner"]

all_words = []
for line in sys.stdin:
    all_words.append(line.strip())

def removeAuto(word):
    for auto in autoList:
        if (word.endswith(auto) and not word == auto):
            return word[:-len(auto)]
    return word


regex = re.compile("\s+|-", re.IGNORECASE)

for loop_1 in all_words:
    word_outer = re.sub(regex,"",loop_1)
    cluster = []
    head_outer = removeAuto(word_outer)
    for loop_2 in all_words:
        word_inner = re.sub(regex,"",loop_2)
        if word_inner == word_outer:
            continue
        if (distance(word_outer,word_inner) < 2 and len(head_outer) > 4):
            cluster.append(loop_2)
    if(len(cluster) > 0):
        for c in cluster:
            all_words.remove(c)
        print(loop_1 + " " + str(cluster))
